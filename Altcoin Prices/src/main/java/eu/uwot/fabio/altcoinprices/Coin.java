package eu.uwot.fabio.altcoinprices;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.Iterator;

class Coin {

    String[] coins = new String[] {
        "BTC",
        "BCH",
        "ETH",
        "LTC",

        "AE",
        "REP",
        "BAT",
        "BNB",
        "BCD",
        "BTG",
        "BTS",
        "BLK",
        "BCN",
        "BTM",
        "ADA",
        "LINK",
        "DASH",
        "DCR",
        "DGD",
        "DOGE",
        "EOS",
        "ETC",
        "GNO",
        "GNT",
        "HSR",
        "KIN",
        "ICX",
        "ICN",
        "IOST",
        "IOT",
        "LSK",
        "LRC",
        "MLN",
        "XMR",
        "NANO",
        "XEM",
        "NEO",
        "OMG",
        "ONT",
        "PART",
        "PPT",
        "QTUM",
        "RHOC",
        "REQ",
        "XRP",
        "SC",
        "SNT",
        "STEEM",
        "XLM",
        "STRAT",
        "TRTL",
        "TRX",
        "VEN",
        "XVG",
        "VRM",
        "WTC",
        "WAN",
        "WAVES",
        "ZEC",
        "ZIL",
        "ZRX",

        "BLX",
        "CCC",
        "SOPR",
        "FCI",
        "GEM",
        "PCC",
        "WMX",
        "BMC",
        "AAAX",
        "CAR",
        "KCOR",
        "CBST",
        "RCAA",
        "BIF",
        "CRNC",
        "BGA15",
        "BEA",
        "BLS",
        "FCE",
        "PPI",
        "JJK",
        "TRADE",
        "EAA",
        "SCX",
        "CCP"
    };
    private final String[] iconomi_daa = new String[]{
        "BLX",
        "CCC",
        "SOPR",
        "FCI",
        "GEM",
        "PCC",
        "WMX",
        "BMC",
        "AAAX",
        "CAR",
        "KCOR",
        "CBST",
        "RCAA",
        "BIF",
        "CRNC",
        "BGA15",
        "BEA",
        "BLS",
        "FCE",
        "PPI",
        "JJK",
        "TRADE",
        "EAA",
        "SCX",
        "CCP"
    };
    String[] descriptions = new String[]{
        "Bitcoin",
        "Bitcoin Cash",
        "Ethereum",
        "Litecoin",

        "Aeternity",
        "Augur",
        "Basic Attention Token",
        "Binance Coin",
        "Bitcoin Diamond",
        "Bitcoin Gold",
        "Bitshares",
        "BlackCoin",
        "Bytecoin",
        "Bytom",
        "Cardano",
        "ChainLink",
        "Dash",
        "Decred",
        "Digix DAO",
        "Dogecoin",
        "EOS",
        "Ethereum Classic",
        "Gnosis",
        "Golem",
        "Hshare",
        "Kin",
        "Icon",
        "Iconomi",
        "IOStoken",
        "IOTA",
        "Lisk",
        "Loopring",
        "Melon",
        "Monero",
        "Nano",
        "NEM",
        "NEO",
        "OmiseGO",
        "Ontology",
        "Particl",
        "Populous",
        "Qtum",
        "RChain",
        "Request Network",
        "Ripple",
        "Siacoin",
        "Status Network Token",
        "Steem",
        "Stellar Lumens",
        "Stratis",
        "TurtleCoin",
        "TRON",
        "VeChain",
        "Verge",
        "Verium",
        "Waltonchain",
        "Wanchain",
        "Waves",
        "Zcash",
        "Zilliqa",
        "0x",

        "Blockchain Index (ICN DAA)",
        "Crush Crypto Core (ICN DAA)",
        "Solidum Prime (ICN DAA)",
        "Future Chain Index (ICN DAA)",
        "Greychain Emerging Markets (ICN DAA)",
        "The Pecunio Cryptocurrency (ICN DAA)",
        "William Mougayar High Growth Cryptoassets Index (ICN DAA)",
        "BMC Original (ICN DAA)",
        "The Asymmetry DAA (ICN DAA)",
        "CARUS-AR (ICN DAA)",
        "KryptoStar CORE (ICN DAA)",
        "Coinbest 1 (ICN DAA)",
        "Ragnarok Crypto Asset Array (ICN DAA)",
        "Blockchain Infrastructure Index (ICN DAA)",
        "Cornucopia Index (ICN DAA)",
        "Global Blockchain Arrays represents (ICN DAA)",
        "Blockchain Easy Access (ICN DAA)",
        "Blockchain Smart (ICN DAA)",
        "Future Crypto Economy (ICN DAA)",
        "Phoenix Paradigm Indicator (ICN DAA)",
        "JJK Crypto Assets (ICN DAA)",
        "Trade (ICN DAA)",
        "Exponential Age Array (ICN DAA)",
        "StrongCoindex (ICN DAA)",
        "Pinta (ICN DAA)"
    };
    String[] coinsLabelDescriptionsString;
    Hashtable<String, String> coinsLabelDescriptionHashtable;
    Hashtable<String, String> coinsDescriptionLabelHashtable;
    private final Hashtable<String, String> coinsLabelExchangeHashtable = new Hashtable<String, String>()
    {{  put("BTC", "coinbase");
        put("BCH", "coinbase");
        put("ETH", "coinbase");
        put("LTC", "coinbase");

        put("AE", "binance");
        put("REP", "bittrex");
        put("BAT", "bittrex");
        put("BNB", "binance");
        put("BCD", "binance");
        put("BTG", "bittrex");
        put("BTS", "bittrex");
        put("BLK", "bittrex");
        put("BCN", "hitbtc");
        put("BTM", "cryptocompare");
        put("ADA", "bittrex");
        put("LINK", "binance");
        put("DASH", "bittrex");
        put("DCR", "bittrex");
        put("DGD", "binance");
        put("DOGE", "bittrex");
        put("EOS", "bitfinex");
        put("ETC", "bittrex");
        put("GNO", "bittrex");
        put("GNT", "bittrex");
        put("HSR", "binance");
        put("KIN", "cryptocompare");
        put("ICX", "binance");
        put("ICN", "kraken");
        put("IOST", "kraken");
        put("IOT", "bitfinex");
        put("LSK", "bittrex");
        put("LRC", "binance");
        put("MLN", "bittrex");
        put("XMR", "bittrex");
        put("NANO", "binance");
        put("XEM", "bittrex");
        put("NEO", "bittrex");
        put("OMG", "bittrex");
        put("ONT", "binance");
        put("PART", "bittrex");
        put("PPT", "binance");
        put("QTUM", "binance");
        put("RHOC", "cryptocompare");
        put("REQ", "binance");
        put("XRP", "bittrex");
        put("SC", "bittrex");
        put("SNT", "binance");
        put("STEEM", "bittrex");
        put("XLM", "bittrex");
        put("STRAT", "bittrex");
        put("TRTL", "cryptocompare");
        put("TRX", "bitfinex");
        put("VEN", "binance");
        put("XVG", "bittrex");
        put("VRM", "bittrex");
        put("WTC", "binance");
        put("WAN", "binance");
        put("WAVES", "binance");
        put("ZEC", "bittrex");
        put("ZIL", "binance");
        put("ZRX", "binance");

        put("BLX", "cryptocompare");
        put("CCC", "cryptocompare_icndaa");
        put("SOPR", "cryptocompare_icndaa");
        put("FCI", "cryptocompare_icndaa");
        put("GEM", "cryptocompare_icndaa");
        put("PCC", "cryptocompare_icndaa");
        put("WMX", "cryptocompare_icndaa");
        put("BMC", "cryptocompare_icndaa");
        put("AAAX", "cryptocompare_icndaa");
        put("CAR", "cryptocompare_icndaa");
        put("KCOR", "cryptocompare_icndaa");
        put("CBST", "cryptocompare_icndaa");
        put("RCAA", "cryptocompare_icndaa");
        put("BIF", "cryptocompare_icndaa");
        put("CRNC", "cryptocompare_icndaa");
        put("BGA15", "cryptocompare_icndaa");
        put("BEA", "cryptocompare_icndaa");
        put("BLS", "cryptocompare_icndaa");
        put("FCE", "cryptocompare_icndaa");
        put("PPI", "cryptocompare_icndaa");
        put("JJK", "cryptocompare_icndaa");
        put("TRADE", "cryptocompare_icndaa");
        put("EAA", "cryptocompare_icndaa");
        put("SCX", "cryptocompare_icndaa");
        put("CCP", "cryptocompare_icndaa");
    }};
    final Hashtable<String, String> coinsLabelGraph = new Hashtable<String, String>()
    {{  put("BTC", "USD");
        put("BCH", "USD");
        put("ETH", "USD");
        put("LTC", "USD");

        put("AE", "BTC");
        put("REP", "USD");
        put("BAT", "USD");
        put("BNB", "USDT");
        put("BCD", "USD");
        put("BTG", "USDT");
        put("BTS", "USDT");
        put("BLK", "USD");
        put("BCN", "USD");
        put("BTM", "na");
        put("ADA", "USDT");
        put("LINK", "USD");
        put("DASH", "USDT");
        put("DCR", "USD");
        put("DGD", "USD");
        put("DOGE", "USD");
        put("EOS", "USD");
        put("ETC", "USDT");
        put("GNO", "USD");
        put("GNT", "USD");
        put("HSR", "BTC");
        put("KIN", "na");
        put("ICX", "USD");
        put("ICN", "USD");
        put("IOST", "BTC");
        put("IOT", "USD");
        put("LSK", "USD");
        put("LRC", "USD");
        put("MLN", "USD");
        put("XMR", "USDT");
        put("NANO", "BTC");
        put("XEM", "USD");
        put("NEO", "USDT");
        put("OMG", "USDT");
        put("ONT", "BTC");
        put("PART", "USD");
        put("PPT", "USD");
        put("QTUM", "USD");
        put("RHOC", "na");
        put("REQ", "USD");
        put("XRP", "USDT");
        put("SC", "USDT");
        put("SNT", "USD");
        put("STEEM", "USD");
        put("XLM", "USD");
        put("STRAT", "USD");
        put("TRX", "USD");
        put("TRTL", "na");
        put("VEN", "USD");
        put("XVG", "USDT");
        put("VRM", "USD");
        put("WTC", "USD");
        put("WAN", "BTC");
        put("WAVES", "USD");
        put("ZEC", "USDT");
        put("ZIL", "BTC");
        put("ZRX", "USD");

        put("BLX", "na");
        put("CCC", "na");
        put("SOPR", "na");
        put("FCI", "na");
        put("GEM", "na");
        put("PCC", "na");
        put("WMX", "na");
        put("BMC", "na");
        put("AAAX", "na");
        put("CAR", "na");
        put("KCOR", "na");
        put("CBST", "na");
        put("RCAA", "na");
        put("BIF", "na");
        put("CRNC", "na");
        put("BGA15", "na");
        put("BEA", "na");
        put("BLS", "na");
        put("FCE", "na");
        put("PPI", "na");
        put("JJK", "na");
        put("TRADE", "na");
        put("EAA", "na");
        put("SCX", "na");
        put("CCP", "na");
    }};
    final String [] currencies = new String[] {
            "USD",
            "EUR",
            "GBP",
            "CNY",
            "JPY",
            "RUB",
            "CAD",
            "AUD",
            "INR",
            "KRW",
            "CHF",
    };

    private final Context context;
    private SharedPreferences prefs;
    private SharedPreferences.Editor editor;
    private float btcusd;
    private float btceur;
    private float btcgbp;
    private float btccny;
    private float btcjpy;
    private float btcrub;
    private float btccad;
    private float btcaud;
    private float btcinr;
    private float btckrw;
    private float btcchf;

    public Coin(Context context, boolean doBTCUSDEUR) {
        this.context = context.getApplicationContext();
        prefs = context.getSharedPreferences("Settings", 0);

        if (doBTCUSDEUR) {
            btcusd = prefs.getFloat("btcusd", 1);
            btceur = prefs.getFloat("btceur", 1);
            btcgbp = prefs.getFloat("btcgbp", 1);
            btccny = prefs.getFloat("btccny", 1);
            btcjpy = prefs.getFloat("btcjpy", 1);
            btcrub = prefs.getFloat("btcrub", 1);
            btccad = prefs.getFloat("btccad", 1);
            btcaud = prefs.getFloat("btcaud", 1);
            btcinr = prefs.getFloat("btcinr", 1);
            btckrw = prefs.getFloat("btckrw", 1);
            btcchf = prefs.getFloat("btcchf", 1);
        }

        initCoins();
    }

    private void initCoins() {
        coinsLabelDescriptionsString = new String[coins.length];
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionsString[i] = coins[i] + " - " + descriptions[i];
        }

        coinsLabelDescriptionHashtable = new Hashtable<>();
        for (int i = 0; i < coins.length; i++) {
            coinsLabelDescriptionHashtable.put(coins[i], descriptions[i]);
        }

        coinsDescriptionLabelHashtable = new Hashtable<>();
        coinsDescriptionLabelHashtable.put("BTC/USD - Bitcoin", "BTCUSD");
        coinsDescriptionLabelHashtable.put("ETH/USD - Ethereum", "ETHUSD");
        for (int i = 0; i < coins.length; i++) {
            coinsDescriptionLabelHashtable.put(coins[i] + " - " + descriptions[i], coins[i]);
        }

        loadCustomCoinsData();
    }

    // Get which exchange trade a coin //
    public String getCoinExchange (String altcoinName) {
        return coinsLabelExchangeHashtable.get(altcoinName);
    }

    public boolean isItIconomiDaa (String altcoinName) {
        for (int i = 0; i < iconomi_daa.length; i++) {
            if (altcoinName.equals(iconomi_daa[i])) {
                return true;
            }
        }

        return false;
    }

    public void addItem(String altcoinDesc, float amountBought, float unitPrice, String currency, long unixTimestamp) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        String altcoinLabel = coinsDescriptionLabelHashtable.get(altcoinDesc);
        float unitPriceBTC;

        if (altcoinLabel.equals("BTC")) {
            unitPriceBTC = 1f;
        } else {
            unitPriceBTC = getCoinValue(altcoinLabel, "BTC", unixTimestamp);
        }

        float amountBought_old = prefs.getFloat(altcoinLabel + "_a", -1f);

        if (amountBought_old != -1f) {
            // Amount can't be a negative number
            if (amountBought_old + amountBought > 0) {
                String altcoinCurrency = prefs.getString(altcoinLabel + "_currency", "EUR");
                float p_btc = prefs.getFloat(altcoinLabel + "_p_btc", 0f);
                float unitPrice_old = prefs.getFloat(altcoinLabel + "_p", -1f);

                // Convert value to the correct currency if needed
                if (!altcoinCurrency.equals(currency)) {
                    unitPrice = currencyToCurrency(unitPrice, altcoinCurrency, currency);
                }

                unitPrice = (amountBought_old * unitPrice_old + amountBought * unitPrice) / (amountBought_old + amountBought);
                unitPriceBTC = ((amountBought_old * p_btc) + (amountBought * unitPriceBTC)) / (amountBought_old + amountBought);
                amountBought = amountBought_old + amountBought;
            } else {
                Toast.makeText(context, R.string.errorNegativeAmount, Toast.LENGTH_SHORT).show();
                return;
            }
        } else {
            editor.putString(altcoinLabel + "_currency", currency);
        }

        editor.putFloat(altcoinLabel + "_p_btc", unitPriceBTC);
        editor.putFloat(altcoinLabel + "_a", amountBought);
        editor.putFloat(altcoinLabel + "_p", unitPrice);
        editor.apply();
    }

    public void editItem(String altcoinLabel, float amountBought, float unitPrice, long unixTimestamp) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        if ((amountBought >= 0) && (unitPrice >= 0)) {
            if ((!altcoinLabel.equals("BTC")) && (unixTimestamp != 0L)) {
                float unitPriceBTC = getCoinValue(altcoinLabel, "BTC", unixTimestamp);
                editor.putFloat(altcoinLabel + "_p_btc", unitPriceBTC);
            }

            editor.putFloat(altcoinLabel + "_a", amountBought);
            editor.putFloat(altcoinLabel + "_p", unitPrice);
            editor.apply();
        } else {
            Toast.makeText(context, R.string.error, Toast.LENGTH_SHORT).show();
        }
    }

    public void removeItem(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);
        editor = prefs.edit();

        editor.remove(altcoinName + "_a");
        editor.remove(altcoinName + "_p");
        editor.remove(altcoinName + "_currentUnitValue");
        editor.remove(altcoinName + "_lastDayUnitValue");
        editor.remove(altcoinName + "_currency");
        editor.remove(altcoinName + "_p_btc");
        editor.remove(altcoinName + "_currentUnitValue_btc");
        editor.remove(altcoinName + "_lastDayUnitValue_btc");

        editor.apply();
    }

    // Get coin value from cryptocompare.com //
    public float getCoinValue(String altcoinName, String currency, long unixTimestamp) {
        float coinValue;

        if (!getCoinExchange(altcoinName).equals("cryptocompare_icndaa")) {
            coinValue = getCoinQuoteCryptoCompare(altcoinName, currency, unixTimestamp);
        } else {
            coinValue = -1f;
        }

        // Exchange API are down or reporting broken values
        if (coinValue == -1f) {
            coinValue = getCoinInitialValue(altcoinName);
        }

        return coinValue;
    }

    // Get coin change in _currency_ from cryptocompare.com //
    // CURRENT    REQUEST: https://min-api.cryptocompare.com/data/price?fsym=BTC&tsyms=USD
    // CURRENT    RESPONSE: {"USD":6945.12}
    // HISTORICAL REQUEST: https://min-api.cryptocompare.com/data/pricehistorical?fsym=BTC&tsyms=USD&ts=1524831212960
    // HISTORICAL RESPONSE: {"BTC":{"USD":9254.98}}
    private float getCoinQuoteCryptoCompare(String altcoinSymbol, String currency, long unixTimestamp) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        StringBuilder dataSTR = new StringBuilder();
        float coinQuote = -1f;

        if (unixTimestamp == 0) {
            try {
                url = new URL("https://min-api.cryptocompare.com/data/price?fsym=" +
                        altcoinSymbol +
                        "&tsyms=" +
                        currency);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        } else {
            try {
                url = new URL("https://min-api.cryptocompare.com/data/pricehistorical?fsym=" +
                        altcoinSymbol +
                        "&tsyms=" +
                        currency +
                        "&ts=" +
                        unixTimestamp);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR.append(Character.toString(current));
            }
        } catch (SocketTimeoutException e) {
            dataSTR = new StringBuilder("0");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        JSONObject json;

        try {
            if (unixTimestamp == 0) {
                json = new JSONObject(dataSTR.toString());
            } else {
                json = new JSONObject(dataSTR.toString()).getJSONObject(altcoinSymbol);
            }

            coinQuote = (float) json.getDouble(currency);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return coinQuote;
    }

    // Get BTC trading pairs from cryptocompare.com //
    // CURRENT    REQUEST: https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC&tsyms=USD,EUR,CND
    // CURRENT    RESPONSE: {"BTC":{"USD":7237.55,"EUR":6199.56,"CND":125313.28}}
    public float[] getBTCQuoteCryptoCompare(String [] currencies) {
        URL url = null;
        HttpURLConnection urlConnection = null;
        StringBuilder dataSTR = new StringBuilder();
        int currenciesNumber = 11;
        float coinQuote[] = new float[currenciesNumber];
        StringBuilder tradingPairsString = new StringBuilder();

        // Build trading pairs String
        for (int i = 0; i< currencies.length - 1; i++) {
            tradingPairsString.append(currencies[i]).append(",");
        }
        tradingPairsString.append(currencies[currenciesNumber - 1]);

        try {
            url = new URL("https://min-api.cryptocompare.com/data/price?fsym=" +
                    "BTC" +
                    "&tsyms=" +
                    tradingPairsString);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        try {
            assert url != null;
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setConnectTimeout(5000); // 5 seconds timeout

            InputStream in = urlConnection.getInputStream();
            InputStreamReader isw = new InputStreamReader(in);

            int data = isw.read();
            while (data != -1) {
                char current = (char) data;
                data = isw.read();
                dataSTR.append(Character.toString(current));
            }
        } catch (SocketTimeoutException e) {
            dataSTR = new StringBuilder("0");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            assert urlConnection != null;
            urlConnection.disconnect();
        }

        if (!dataSTR.toString().equals("")) {
            JSONObject json;

            try {
                json = new JSONObject(dataSTR.toString());

                for (int i = 0; i < currenciesNumber; i++) {
                    coinQuote[i] = (float) json.getDouble(currencies[i]);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            for (int i = 0; i < currenciesNumber; i++) {
                coinQuote[i] = 1;
            }
        }

        return coinQuote;
    }

    // Convert currencies //
    public float currencyToCurrency(float price, String currency, String newCurrency) {
        float change0 = getChange(currency);
        float change1 = getChange(newCurrency);

        return price * change1 / change0;
    }

    // Get trading pair //
    private float getChange(String currency) {
        float change = -1f;

        switch (currency) {
            case "USD":
                change = btcusd;
                break;
            case "EUR":
                change = btceur;
                break;
            case "GBP":
                change = btcgbp;
                break;
            case "CNY":
                change = btccny;
                break;
            case "JPY":
                change = btcjpy;
                break;
            case "RUB":
                change = btcrub;
                break;
            case "CAD":
                change = btccad;
                break;
            case "AUD":
                change = btcaud;
                break;
            case "INR":
                change = btcinr;
                break;
            case "KRW":
                change = btckrw;
                break;
            case "CHF":
                change = btcchf;
                break;
            case "BTC":
                change = 1f;
                break;
        }

        return change;
    }

    // Get the amount of FIAT a coin was paid //
    private float getCoinInitialValue(String altcoinName) {
        prefs = context.getApplicationContext().getSharedPreferences("Settings", 0);

        return prefs.getFloat(altcoinName + "_p", -1f);
    }

    // Load portfolio items array //
    private void loadCustomCoinsData() {
        String jsonData = prefs.getString("customCoins", "");

        if (jsonData.length() != 0) {
            try {
                JSONObject json = new JSONObject(jsonData);
                int size = json.length();

                if (size >= 1) {
                    int firstFree = coins.length;
                    expandArrays(size);
                    Iterator<?> keys = json.keys();

                    while (keys.hasNext()) {
                        String altcoinSymbol, altcoinDescription, tradingviewExchangeName, tradingviewTradingPair;
                        String key = (String) keys.next();

                        JSONObject jsonCoinDetails = json.getJSONObject(key);
                        altcoinSymbol = jsonCoinDetails.getString("altcoinSymbol");
                        altcoinDescription = jsonCoinDetails.getString("altcoinDescription");
                        tradingviewExchangeName = jsonCoinDetails.getString("tradingviewExchangeName");
                        tradingviewTradingPair = jsonCoinDetails.getString("tradingviewTradingPair");

                        addCustomCoin(altcoinSymbol,
                                altcoinDescription,
                                tradingviewExchangeName,
                                tradingviewTradingPair,
                                firstFree);

                        firstFree++;
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    // Exapand arrays size to fit custom coins //
    private void expandArrays(int size) {
        coins = Arrays.copyOf(coins, coins.length + size);
        descriptions = Arrays.copyOf(descriptions, descriptions.length + size);
        coinsLabelDescriptionsString = Arrays.copyOf(coinsLabelDescriptionsString, coinsLabelDescriptionsString.length + size);
    }

    // Add custom coin //
    private void addCustomCoin(String altcoinSymbol, String altcoinDescription,
                               String tradingviewExchangeName, String tradingviewTradingPair,
                               int firstFree) {
        coins[firstFree] = altcoinSymbol;
        descriptions[firstFree] = altcoinDescription;
        coinsLabelDescriptionsString[firstFree] = coins[firstFree] + " - " + descriptions[firstFree];
        coinsLabelDescriptionHashtable.put(coins[firstFree], descriptions[firstFree]);
        coinsDescriptionLabelHashtable.put(coins[firstFree] + " - " + descriptions[firstFree], coins[firstFree]);

        coinsLabelExchangeHashtable.put(coins[firstFree], tradingviewExchangeName);
        coinsLabelGraph.put(coins[firstFree], tradingviewTradingPair);
    }

}
